package max.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import max.func.Methods;
import max.gui.panels.AproxPanel;
import max.gui.panels.DihotomiaPanel;
import max.gui.panels.GausPanel;
import max.gui.panels.GoldenRatioPanel;
import max.gui.panels.HordaPanel;
import max.gui.panels.IntergralPanel;
import max.gui.panels.InterpolPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setTitle("Чисельні методи Нечаєв Максим КС-32");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 635, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel main_panel = new JPanel();
		tabbedPane.addTab("Корни", null, main_panel, null);
		main_panel.setLayout(new BorderLayout(0, 0));

		// HERE
		// chart_panel = new JPanel();
		// main_panel.add(chart_panel);

		JPanel control_panel = new JPanel();
		main_panel.add(control_panel, BorderLayout.NORTH);
		control_panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel label = new JLabel("От:");
		control_panel.add(label);

		JTextField from_txt = new JTextField();
		control_panel.add(from_txt);
		from_txt.setColumns(10);

		JLabel label_1 = new JLabel("До:");
		control_panel.add(label_1);

		JTextField to_txt = new JTextField();
		control_panel.add(to_txt);
		to_txt.setColumns(10);

		JLabel label_2 = new JLabel("Шаг:");
		control_panel.add(label_2);

		JTextField step_txt = new JTextField();
		control_panel.add(step_txt);
		step_txt.setColumns(10);
		
		JPanel panel = new JPanel();
		main_panel.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JTextPane root_text = new JTextPane();
		root_text.setText("Корни:");
		panel.add(root_text);
		
		JButton btnDraw = new JButton("Calc");
		btnDraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double from	 = Double.parseDouble(from_txt.getText());
				double to	 = Double.parseDouble(to_txt.getText());
				double step	 = Double.parseDouble(step_txt.getText());				
				root_text.setText(Methods.roots(from, to, step, null).toString());
			}
		});
		control_panel.add(btnDraw);
		
		JButton btnDrae = new JButton("Draw");
		btnDrae.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double from	 = Double.parseDouble(from_txt.getText());
				double to	 = Double.parseDouble(to_txt.getText());
				double step	 = Double.parseDouble(step_txt.getText());	
				ChartVisual.visualize(from, to, step);
			}
		});
		control_panel.add(btnDrae);

		DihotomiaPanel dihotomia_panel = new DihotomiaPanel();
		tabbedPane.addTab("Дихотомия", null, dihotomia_panel, null);
		
		HordaPanel panel_1 = new HordaPanel();
		tabbedPane.addTab("Хорды", null, panel_1, null);
		
		GausPanel Gaus_panel = new GausPanel();
		tabbedPane.addTab("Гаус", null, Gaus_panel, null);
		
		InterpolPanel panel_2 = new InterpolPanel();
		tabbedPane.addTab("Интерполяция", null, panel_2, null);
		
		AproxPanel aprox_panel = new AproxPanel();
		tabbedPane.addTab("Апроксимація", null, aprox_panel, null);
		
		IntergralPanel integral_panel = new IntergralPanel();
		tabbedPane.addTab("Інтеграл", null, integral_panel, null);
		
		GoldenRatioPanel goldCut_panel = new GoldenRatioPanel();
		tabbedPane.addTab("Золотий перетин", null, goldCut_panel, null);
	}
}
