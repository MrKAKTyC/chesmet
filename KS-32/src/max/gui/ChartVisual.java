package max.gui;

import java.util.LinkedList;

import javax.swing.JFrame;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.markers.SeriesMarkers;

import max.func.Function;

public abstract class ChartVisual {

	public static void visualize(double from, double to, double step) {
		LinkedList<Double> x_list = new LinkedList<>();
		LinkedList<Double> Fx_list = new LinkedList<>();
		Function fun = new Function();

		for (double d = from; d < to; d += step) {
			x_list.add(d);
			Fx_list.add(fun.f1(d, null));
		}
		double[] xData = new double[x_list.size()];
		double[] yData = new double[Fx_list.size()];
		for (int i = 0; i < x_list.size(); i++) {
			xData[i] = x_list.get(i);
			yData[i] = Fx_list.get(i);
		}
		Thread t = null;
		XYChart chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", xData, yData);
		if (t == null) {
			t = new Thread(new Runnable() {
				@SuppressWarnings("rawtypes")
				SwingWrapper sw;

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public void run() {
					if (sw == null) {
						sw = new SwingWrapper(chart);
						sw.displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						;
					} else {
						sw.repaintChart();
					}
				}

			});
			t.start();
		}
	}

	public static void visualize(String[] chartNames, double[][] Xseries, double[][] Yseries) {

		Thread t = null;
		XYChart chart = new XYChartBuilder().width(800).height(600).title("Interpolation").build();
		for (int i = 0; i < Xseries.length; i++) {
			chart.addSeries(chartNames[i], Xseries[i], Yseries[i]).setMarker(SeriesMarkers.NONE);
		}
		if (t == null) {
			t = new Thread(new Runnable() {
				@SuppressWarnings("rawtypes")
				SwingWrapper sw;

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public void run() {
					if (sw == null) {
						sw = new SwingWrapper(chart);
						sw.displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						;
					} else {
						sw.repaintChart();
					}
				}

			});
			t.start();
		}
	}
	
	public static void visualize(final XYChart chart) {
		Thread t = null;
		if (t == null) {
			t = new Thread(new Runnable() {
				@SuppressWarnings("rawtypes")
				SwingWrapper sw;

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public void run() {
					if (sw == null) {
						sw = new SwingWrapper(chart);
						sw.displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						;
					} else {
						sw.repaintChart();
					}
				}
			});
			t.start();
		}
	}

}
