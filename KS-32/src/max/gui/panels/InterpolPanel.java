package max.gui.panels;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import javax.swing.JLabel;
import javax.swing.JTextField;

import max.func.Interpolation;
import max.gui.ChartVisual;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterpolPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3282647217988284291L;
	private JTextField txtX;
	private JTextField txtFx;
	private JLabel lblPoly;

	/**
	 * Create the panel.
	 */
	public InterpolPanel() {
		setLayout(new MigLayout("", "[][grow]", "[][][][]"));

		JLabel lblX = new JLabel("x:");
		add(lblX, "cell 0 0,alignx trailing");

		txtX = new JTextField();
		add(txtX, "cell 1 0,growx");
		txtX.setColumns(10);

		JLabel lblFx = new JLabel("f(x):");
		add(lblFx, "cell 0 1,alignx trailing");

		txtFx = new JTextField();
		add(txtFx, "cell 1 1,growx");
		txtFx.setColumns(10);

		JButton btnCalc = new JButton("calc");
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] strX = txtX.getText().split(" ");
				String[] strFx = txtFx.getText().split(" ");
				double[] dbX = new double[strX.length];
				double[] dbFx = new double[strFx.length];
				for (int i = 0; i < strX.length; i++) {
					dbX[i] = Double.parseDouble(strX[i]);
					dbFx[i] = Double.parseDouble(strFx[i]);
				}
				Interpolation interpol = new Interpolation();
				interpol.setFunc(dbFx, dbX);
				interpol.calculate();
				Expression e = new ExpressionBuilder(interpol.toString().replaceAll(",", "."))
				        .variables("x")
				        .build();
				double[] plX = new double[1000];
				double[] plFx = new  double[1000];
				double step = (dbX[dbX.length-1]-dbX[0])/1000;
				for(int i = 0; i < 1000; i++) {
					plX[i] = dbX[0]+step*i;
					e.setVariable("x", plX[i]);
					plFx[i]= e.evaluate();
				}
				lblPoly.setText(interpol.toString());
				ChartVisual.visualize(new String[]{"f(x)", "polinom"}, new double[][]{dbX, plX}, new double[][] {dbFx, plFx});
			}
		});
		add(btnCalc, "cell 0 2");

		JLabel label = new JLabel("Поліном");
		add(label, "cell 0 3");

		lblPoly = new JLabel("");
		add(lblPoly, "cell 1 3");

	}

}
