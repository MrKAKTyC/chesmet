package max.gui.panels;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

import max.func.GoldenRatio;
import max.func.IFunction;
import net.objecthunter.exp4j.ExpressionBuilder;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class GoldenRatioPanel extends JPanel {
	private static final long serialVersionUID = 1031633249703877853L;
	private JTextField txtFrom;
	private JTextField txtTo;
	private JTextField textField;
	private JTextField txtFunc;
	private JTextField txtE;

	/**
	 * Create the panel.
	 */
	public GoldenRatioPanel() {
		SpringLayout springLayout = new SpringLayout();
		setLayout(springLayout);

		JLabel lblFrom = new JLabel("From");
		springLayout.putConstraint(SpringLayout.NORTH, lblFrom, 10, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, lblFrom, 10, SpringLayout.WEST, this);
		add(lblFrom);

		txtFrom = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, txtFrom, 6, SpringLayout.SOUTH, lblFrom);
		springLayout.putConstraint(SpringLayout.WEST, txtFrom, 10, SpringLayout.WEST, this);
		add(txtFrom);
		txtFrom.setColumns(10);

		txtTo = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, txtTo, 6, SpringLayout.EAST, txtFrom);
		springLayout.putConstraint(SpringLayout.SOUTH, txtTo, 0, SpringLayout.SOUTH, txtFrom);
		add(txtTo);
		txtTo.setColumns(10);

		JLabel lblTo = new JLabel("To");
		springLayout.putConstraint(SpringLayout.WEST, lblTo, 0, SpringLayout.WEST, txtTo);
		springLayout.putConstraint(SpringLayout.SOUTH, lblTo, 0, SpringLayout.SOUTH, lblFrom);
		add(lblTo);

		JButton btnCalc = new JButton("Calc");
		springLayout.putConstraint(SpringLayout.WEST, btnCalc, -102, SpringLayout.EAST, this);
		springLayout.putConstraint(SpringLayout.EAST, btnCalc, -16, SpringLayout.EAST, this);
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double from, to, err;
				String strFunc = txtFunc.getText();
				IFunction func = new IFunction() {
					@Override
					public double calculate(double x) {
						return new ExpressionBuilder(strFunc).variables("x").build().setVariable("x", x).evaluate();
					}
				};
				from = Double.parseDouble(txtFrom.getText());
				to = Double.parseDouble(txtTo.getText());
				err = Double.parseDouble(txtE.getText());
				GoldenRatio gr = new GoldenRatio(from, to, err, func);
				textField.setText(new Double(gr.calcalate()).toString());
			}
		});
		add(btnCalc);

		JLabel lblResult = new JLabel("Result");
		springLayout.putConstraint(SpringLayout.WEST, lblResult, 0, SpringLayout.WEST, lblFrom);
		add(lblResult);

		textField = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, btnCalc, -1, SpringLayout.NORTH, textField);
		springLayout.putConstraint(SpringLayout.NORTH, textField, 6, SpringLayout.SOUTH, lblResult);
		springLayout.putConstraint(SpringLayout.WEST, textField, 0, SpringLayout.WEST, lblFrom);
		springLayout.putConstraint(SpringLayout.EAST, textField, 0, SpringLayout.EAST, txtTo);
		textField.setEditable(false);
		add(textField);
		textField.setColumns(10);

		txtFunc = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, lblResult, 6, SpringLayout.SOUTH, txtFunc);
		springLayout.putConstraint(SpringLayout.WEST, txtFunc, 10, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.EAST, txtFunc, 280, SpringLayout.WEST, this);
		txtFunc.setText("sin(x)");
		add(txtFunc);
		txtFunc.setColumns(10);

		JLabel lblFx = new JLabel("f(x)");
		springLayout.putConstraint(SpringLayout.NORTH, txtFunc, 6, SpringLayout.SOUTH, lblFx);
		springLayout.putConstraint(SpringLayout.NORTH, lblFx, 6, SpringLayout.SOUTH, txtFrom);
		springLayout.putConstraint(SpringLayout.WEST, lblFx, 0, SpringLayout.WEST, lblFrom);
		add(lblFx);
		
		txtE = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, txtE, 6, SpringLayout.EAST, txtTo);
		springLayout.putConstraint(SpringLayout.SOUTH, txtE, 0, SpringLayout.SOUTH, txtFrom);
		add(txtE);
		txtE.setColumns(10);
		
		JLabel lblErr = new JLabel("err");
		springLayout.putConstraint(SpringLayout.WEST, lblErr, 80, SpringLayout.EAST, lblTo);
		springLayout.putConstraint(SpringLayout.SOUTH, lblErr, 0, SpringLayout.SOUTH, lblFrom);
		add(lblErr);

	}
}
