package max.gui.panels;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import max.func.Gaus;

import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.Formatter;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;

public class GausPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3514556493543992265L;
	private JTextField textField;
	private JPanel slau_panel;
	private JPanel panel;
	private JPanel ans_panel;
	private JButton calc;

	private Gaus g;

	/**
	 * Create the panel.
	 */
	public GausPanel() {
		setLayout(new BorderLayout(0, 0));

		panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		add(panel, BorderLayout.NORTH);

		JLabel lblEhfdytybq = new JLabel("Количество уравнений");
		panel.add(lblEhfdytybq);

		textField = new JTextField();
		textField.setText("3");
		panel.add(textField);
		textField.setColumns(10);

		JButton build = new JButton("Построить");
		build.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int size;
				try {
					size = Integer.parseInt(textField.getText());
				} catch (NumberFormatException e) {
					System.err.println(e.toString());
					return;
				}
				g = new Gaus(size);
				slau_panel.removeAll();
				ans_panel.removeAll();
				slau_panel.setLayout(new GridLayout(size, size * 2 + 1));
				ans_panel.setLayout(new GridLayout(size, 1));
				for (int i = 0; i < size; i++) {
					int x = 1;
					for (int j = 0; j < size * 2 + 1; j++) {
						if (j % 2 == 0) {
							slau_panel.add(new JTextField());
						} else {
							if (x != size) {
								slau_panel.add(new JLabel("x" + x + " + "));
							} else {
								slau_panel.add(new JLabel("x" + x + " = ")); 
							}
							x++;
						}
					}
					ans_panel.add(new JLabel("x" + (i + 1) + ": "));
				}
				calc.setVisible(true);
				revalidate();
			}
		});
		panel.add(build);

		calc = new JButton("Посчитать");
		calc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int size = g.getSize();
				double str[][] = new double[size][size + 1];
				int comp_indx = 0;
				for (int i = 0; i < size; i++) {
					for (int j = 0; j < size + 1; j++) {
								str[i][j] = Double.parseDouble(((JTextField) slau_panel.getComponent(comp_indx)).getText());
						if (j + 1 < size + 1) {
							comp_indx += 2;
						}
					}
					++comp_indx;
				}
				g.setArr(str);
				double[] ans = g.calculate();
				Formatter f = null;
				for(int i =0; i < ans.length; i++) {
					f = new Formatter();
					f.format("%.4f", ans[i]);
					JLabel tmp = (JLabel)ans_panel.getComponent(i);
					tmp.setText(tmp.getText() + " " + f.toString());
				}
				f.close();
			}
		});
		calc.setVisible(false);
		panel.add(calc);

		slau_panel = new JPanel();
		slau_panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(slau_panel, BorderLayout.CENTER);
		slau_panel.setLayout(new GridLayout(1, 0, 0, 0));

		ans_panel = new JPanel();
		ans_panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(ans_panel, BorderLayout.EAST);

	}

}
