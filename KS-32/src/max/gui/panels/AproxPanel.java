package max.gui.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import max.func.Aproximation;
import net.miginfocom.swing.MigLayout;

public class AproxPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3282647217988284291L;
	private JTextField txtX;
	private JTextField txtFx;
	private JLabel lblPoly;

	/**
	 * Create the panel.
	 */
	public AproxPanel() {
		setLayout(new MigLayout("", "[][grow]", "[][][][]"));

		JLabel lblX = new JLabel("x:");
		add(lblX, "cell 0 0,alignx trailing");

		txtX = new JTextField();
		add(txtX, "cell 1 0,growx");
		txtX.setColumns(10);

		JLabel lblFx = new JLabel("f(x):");
		add(lblFx, "cell 0 1,alignx trailing");

		txtFx = new JTextField();
		add(txtFx, "cell 1 1,growx");
		txtFx.setColumns(10);

		JButton btnCalc = new JButton("calc");
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] strX = txtX.getText().split(" ");
				String[] strFx = txtFx.getText().split(" ");
				double[] dbX = new double[strX.length];
				double[] dbFx = new double[strFx.length];
				for (int i = 0; i < strX.length; i++) {
					dbX[i] = Double.parseDouble(strX[i]);
					dbFx[i] = Double.parseDouble(strFx[i]);
				}
				
				Aproximation aprox = new Aproximation(dbX, dbFx);
				aprox.aproximate();
				lblPoly.setText(aprox.getAproximateFunc());
			}
		});
		add(btnCalc, "cell 0 2");

		JLabel label = new JLabel("Функція");
		add(label, "cell 0 3");

		lblPoly = new JLabel("");
		add(lblPoly, "cell 1 3");

	}

}
