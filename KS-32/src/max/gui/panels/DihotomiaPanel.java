package max.gui.panels;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import max.func.Methods;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import javax.swing.JTextPane;

public class DihotomiaPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3253837431928259093L;
	private JTextField from_field;
	private JTextField to_field;
	private JTextField step_field;
	private JTextPane answ_Pane;

	/**
	 * Create the panel.
	 */
	public DihotomiaPanel() {
		setLayout(new BorderLayout(0, 0));

		JPanel cntrl_panel = new JPanel();
		add(cntrl_panel, BorderLayout.NORTH);

		JLabel label = new JLabel("От:");
		cntrl_panel.add(label);

		from_field = new JTextField();
		cntrl_panel.add(from_field);
		from_field.setColumns(10);

		JLabel label_1 = new JLabel("До:");
		cntrl_panel.add(label_1);

		to_field = new JTextField();
		cntrl_panel.add(to_field);
		to_field.setColumns(10);

		JLabel label_2 = new JLabel("Погрешность:");
		cntrl_panel.add(label_2);

		step_field = new JTextField();
		cntrl_panel.add(step_field);
		step_field.setColumns(10);

		JButton button = new JButton("Calc");
		cntrl_panel.add(button);

		answ_Pane = new JTextPane();
		add(answ_Pane, BorderLayout.CENTER);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double from = Double.parseDouble(getFrom()), to = Double.parseDouble(getTo()),
						step = Double.parseDouble(getStep());
				setAnsw("Корень: " + Methods.dixotomia(from, to, step, null) + " ± " + step);
			}
		});

	}

	private String getFrom() {
		return this.from_field.getText();
	}

	private String getTo() {
		return this.to_field.getText();
	}

	private String getStep() {
		return this.step_field.getText();
	}

	private void setAnsw(String answ) {
		this.answ_Pane.setText(answ);
	}

}
