package max.gui.panels;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

import max.func.IFunction;
import max.func.Integral;
import net.objecthunter.exp4j.ExpressionBuilder;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IntergralPanel extends JPanel {

	private static final long serialVersionUID = -2967978507094961244L;
	private JTextField txtFrom;
	private JTextField txtTo;
	private JTextField txtStep;
	private JTextField txtFunc;

	/**
	 * Create the panel.
	 */
	public IntergralPanel() {
		setLayout(null);
		
		txtFrom = new JTextField();
		txtFrom.setText("0");
		txtFrom.setBounds(10, 81, 58, 20);
		add(txtFrom);
		txtFrom.setColumns(10);
		
		txtTo = new JTextField();
		txtTo.setText("2");
		txtTo.setBounds(10, 23, 58, 20);
		add(txtTo);
		txtTo.setColumns(10);
		
		txtStep = new JTextField();
		txtStep.setBounds(66, 116, 86, 20);
		txtStep.setText("100");
		add(txtStep);
		txtStep.setColumns(10);
		
		txtFunc = new JTextField();
		txtFunc.setBounds(46, 54, 104, 20);
		txtFunc.setText("sin(x)");
		add(txtFunc);
		txtFunc.setColumns(10);
		
		JLabel label = new JLabel("∫");
		label.setBounds(20, 41, 35, 37);
		label.setFont(new Font("Tahoma", Font.PLAIN, 30));
		add(label);
		
		JLabel lblDx = new JLabel("dx = ");
		lblDx.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblDx.setBounds(160, 55, 280, 14);
		add(lblDx);
		
		JLabel lblStep = new JLabel("Кількість");
		lblStep.setBounds(10, 119, 46, 14);
		add(lblStep);
		
		JButton btnCalc = new JButton("calc");
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double from, to, step;
				String strFunc = txtFunc.getText();
				IFunction func = new IFunction() {
					@Override
					public double calculate(double x) {
						return new ExpressionBuilder(strFunc)
				                .variables("x").build().setVariable("x", x).evaluate();
					}
				};
				from = Double.parseDouble(txtFrom.getText());
				to = Double.parseDouble(txtTo.getText());
				step = Double.parseDouble(txtStep.getText());
				Integral itg = new Integral(from, to, step, func);
				lblDx.setText(lblDx.getText() + itg.calcalate());
			}
		});
		btnCalc.setBounds(162, 115, 89, 23);
		add(btnCalc);

	}
}
