package max.gui.panels;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;

import max.func.Methods;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HordaPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -148256702933590643L;
	private JTextField from_Field;
	private JTextField to_Field;
	private JTextField step_Field;
	private JTextPane answ_Pane;

	/**
	 * Create the panel.
	 */
	public HordaPanel() {
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);

		JLabel label = new JLabel("От:");
		panel.add(label);

		from_Field = new JTextField();
		panel.add(from_Field);
		from_Field.setColumns(10);

		JLabel label_1 = new JLabel("До:");
		panel.add(label_1);

		to_Field = new JTextField();
		panel.add(to_Field);
		to_Field.setColumns(10);

		JLabel label_2 = new JLabel("Погрешность:");
		panel.add(label_2);

		step_Field = new JTextField();
		panel.add(step_Field);
		step_Field.setColumns(10);

		JButton btn_Calc = new JButton("Calc");
		btn_Calc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double from = Double.parseDouble(getFrom()), to = Double.parseDouble(getTo()),
						step = Double.parseDouble(getStep());
				setAnsw("Корень: " + Methods.method_chord(from, to, step, null) + " ± " + step);
			}
		});
		panel.add(btn_Calc);

		answ_Pane = new JTextPane();
		add(answ_Pane, BorderLayout.CENTER);

	}

	private String getFrom() {
		return this.from_Field.getText();
	}

	private String getTo() {
		return this.to_Field.getText();
	}

	private String getStep() {
		return this.step_Field.getText();
	}

	private void setAnsw(String answ) {
		this.answ_Pane.setText(answ);
	}

}
