package max.func;

import java.util.ArrayList;
import java.util.List;

public abstract class Methods {

	public static List<Section> roots(double from, double to, double step, double[] p) {
		Function fun = new Function();
		ArrayList<Section> answ = new ArrayList<>();

		double value = fun.f1(from, p);
		double newValue;
		for (; from < to;) {
			newValue = fun.f1(from += step, p);
			if (value * newValue <= 0) {
				answ.add(new Section(from - step, from));
			} 
			value = newValue;
		}
		return answ;
	}

	public static double method_chord(double from, double to, double step, double[] params) {
		Function fun = new Function();
		while (Math.abs(to - from) > step) {
			from = to - (to - from) * fun.f1(to, params) / (fun.f1(to, params) - fun.f1(from, params));
			to = from + (from - to) * fun.f1(from, params) / (fun.f1(from, params) - fun.f1(to, params));
		}
		 System.out.println("Calls: "+fun.getCalls());
		return to;
	}

	public static double dixotomia(double from, double to, double step, double[] params) {
		Function fun = new Function();
		double x = 0, c;
		while (Math.abs(from - to) > step) {
			c = (from + to) / 2;
			if (fun.f1(from, params) * fun.f1(c, params) <= 0)
				to = c;
			else {
				from = c;
				x = (from + to) / 2;
			}
		}
		 System.out.println("Calls: "+fun.getCalls());
		return x;
	}
}