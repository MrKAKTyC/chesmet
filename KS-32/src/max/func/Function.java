package max.func;

public class Function {
	private int calls;

	public Function() {
		this.calls = 0;
	}

	public double f1(double x, double[] params) {
		this.calls++;
		if (params != null && params.length != 0) {
			return Math.pow(x, params[0]) - 0.2;
		} else {
			return 1.0 / 3 - Math.pow(x, 2) + 1.0 / 6 * Math.pow(x, 3);
		}
	}

	public int getCalls() {
		return this.calls;
	}

}
