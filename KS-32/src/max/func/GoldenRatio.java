package max.func;

public class GoldenRatio {
	private final static double PHI = (1 + Math.sqrt(5)) / 2;
	private double begin;
	private double end;
	private double e;
	private IFunction func;
	private double ansv;

	public GoldenRatio(double begin, double end, double e, IFunction func) {
		this.begin = begin;
		this.end = end;
		this.e = e;
		this.func = func;
		this.ansv = 0;
	}

	public double calcalate() {
		double x1, y1;
		while (end - begin > e) {
			x1 = begin + ((end - begin) / PHI);
			y1 = func.calculate(x1);
			if (y1 >= func.calculate(end)) {
				begin = x1;
			} else {
				end = x1;
			}
		}
		ansv = (begin + end) / 2;
		return ansv;
	}
	

	public double getAnsv() {
		return ansv;
	}

	public static void main(String[] args) {
		GoldenRatio gr = new GoldenRatio(0.5, 3, 0.001, (x)->Math.sin(x));
		System.out.println(gr.calcalate());
	}

}

//	public double calcalate_2p() {
//		double x1, x2, y1, y2;
//		System.out.println(begin + "    " + end);
//		while (end - begin > e) {
//			x1 = end - ((end - begin) / PHI);
//			x2 = begin + ((end + begin) / PHI);
//			System.out.println("x1:x2\t" + x1 + "\t" + x2);
//			y1 = func.calculate(x1);
//			y2 = func.calculate(x2);
//			if (y1 >= y2) {
//				begin = x1;
//			} else {
//				end = x2;
//			}
//			System.out.println("b:e\t" + begin + "\t" + end);
//		}
//		ansv = ((begin + end) / 2);
//		return ansv;
//	}