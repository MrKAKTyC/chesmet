package max.func;

public class Section {
	private double from;
	private double to;

	public Section(double to, double from) {
		this.to = to;
		this.from = from;
	}
	
	public double getFrom() {
		return this.from;
	}
	public double getTo() {
		return this.to;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "["+this.to+" ; "+this.from+"]";
	}

}
