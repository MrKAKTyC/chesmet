package max.func;

public class Interpolation {

	private double[] func_x;
	private double[] x;
	private double[] polinom;
	private int size;

//	public static void main(String[] args) throws IOException {
//		Interpolation interpol = new Interpolation();
//		double[] points = new double[] { 0, Math.PI / 2, Math.PI, 3 * Math.PI / 2, 2 * Math.PI, 5 * Math.PI / 2 };
//		double[] val_in_points = new double[points.length];
//		for (int i = 0; i < points.length; i++) {
//			val_in_points[i] = Math.sin(points[i]);
//		}
//		interpol.setFunc(val_in_points, points);
//		interpol.calculate();
//		System.out.println(interpol.toString());
//		for (double d : points) {
//			System.out.printf("%.4f <> %.4f\n", Math.sin(d), function(d));
//		}
//		IFunction sin = (x) -> Math.sin(x);
//		IFunction pol = (x) -> function(x);
//		 ChartVisual.visualize(0, 5 * Math.PI / 2, 0.01, "sin", sin, pol);
//	}

//	public static double function(double x) {
//		return 1.1884 * x - 0.1351 * Math.pow(x, 2) - 0.2150 * Math.pow(x, 3) + 0.0548 * Math.pow(x, 4)
//				- 0.0035 * Math.pow(x, 5);
//	}

	public void setFunc(double[] f_x, double[] x) {
		if (f_x.length != x.length) {
			throw new RuntimeException("wrong arays size");
		}
		this.func_x = f_x;
		this.x = x;
		this.size = func_x.length;

	}

	public double[][] fillMatrix() {
		double[][] matrix = new double[size][size + 1];
		for (int i = 0; i < size; i++)
			matrix[i][0] = 1;
		for (int i = 0; i < size; i++)
			for (int j = 1; j < size; j++)
				matrix[i][j] = matrix[i][j - 1] * x[i];
		for (int i = 0; i < size; i++)
			matrix[i][size] = this.func_x[i];
		return matrix;
	}

	public void calculate() {
		Gaus gaus = new Gaus(this.size);
		gaus.setArr(this.fillMatrix());
		this.polinom = gaus.calculate();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
//		builder.append("P(x) = ");
		for (int i = 0; i < polinom.length; i++) {
			if (polinom[i] == 0) {
				builder.append(0 + " + ");
			} else {
				builder.append(String.format("%.4f*x^%d + ", polinom[i], i));

			}
		}
		builder.delete(builder.length() - 2, builder.length());
		return builder.toString();
	}

	
	public double[] getPolinom() {
		return polinom;
	}


}
