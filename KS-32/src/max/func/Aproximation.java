package max.func;

import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.lines.SeriesLines;
import org.knowm.xchart.style.markers.SeriesMarkers;

import max.gui.ChartVisual;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class Aproximation {

	public static void main(String args[]) {
		Aproximation aprox = new Aproximation(new double[] { 0, 1, 2, 4, 5 }, new double[] { 2.1, 2.4, 2.6, 2.8, 3 });
		aprox.aproximate();

	}

	private String aproximatedFunc;
	private int size;
	private double[] x;
	private double[] y;
	private double[] x_pow;
	private double[] x_y_multiplay;

	public Aproximation(double[] x, double[] y) {
		this.aproximatedFunc = "";
		if (x.length != y.length) {
			throw new RuntimeException("Wrong size param aray");
		}
		this.size = x.length;
		this.x = x;
		this.y = y;
		this.x_pow = new double[x.length];
		this.x_y_multiplay = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			this.x_pow[i] = Math.pow(x[i], 2);
			this.x_y_multiplay[i] = this.x[i] * this.y[i];
		}
	}

	public void aproximate() {
		double a = 0, b = 0;
		a = (this.size * sum(this.x_y_multiplay) - sum(this.x) * sum(this.y))
				/ (this.size * sum(this.x_pow) - Math.pow(sum(this.x), 2));
		b = (sum(this.y) - a * sum(this.x)) / this.size;
		this.aproximatedFunc = a + "*x + " + b;
		this.visualize();
	}

	private double sum(double[] array) {
		double sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum;
	}

	private void visualize() {
		Expression e = new ExpressionBuilder(this.aproximatedFunc.replaceAll(",", ".")).variables("x").build();
		double[] apX = new double[1000];
		double[] apFx = new double[1000];
		double step = (this.x[this.x.length - 1] - this.x[0]) / 1000;
		for (int i = 0; i < 1000; i++) {
			apX[i] = this.x[0] + step * i;
			e.setVariable("x", apX[i]);
			apFx[i] = e.evaluate();
		}
		XYChart chart = new XYChartBuilder().width(800).height(600).title("Aproximation").build();
		chart.addSeries("points", x, y).setLineStyle(SeriesLines.NONE);
		chart.addSeries("aprox", apX, apFx).setMarker(SeriesMarkers.NONE);
		ChartVisual.visualize(chart);
	}

	public String getAproximateFunc() {
		return this.aproximatedFunc;
	}

}
