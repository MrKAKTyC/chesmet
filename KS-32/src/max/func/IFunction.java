package max.func;

public interface IFunction {
	
	double calculate(double x);

}
