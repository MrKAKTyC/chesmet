package max.func;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

public class Main {

	private static final String METHODS = "1:Отделения корней\n2:Хорд\n3:Дихотомии\n0:выход";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String args[]) {
	    LinkedList<Double> x_list = new LinkedList<>();
	    LinkedList<Double> Fx_list = new LinkedList<>();
	    Function fun = new Function();
		for (double d = -2; d < 7; d+=0.01) {
			x_list.add(d);
			Fx_list.add(fun.f1(d, null));
		}
		double[] xData = new double[x_list.size()];
		double[] yData = new double[Fx_list.size()];
		for(int i = 0; i < x_list.size(); i++) {
			xData[i] = x_list.get(i);
			yData[i] = Fx_list.get(i);
		}
		XYChart chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", xData, yData);
		new SwingWrapper(chart).displayChart();
		
		CUI();

	}

	@SuppressWarnings("resource")
	public static void CUI() {
		Scanner scanner = new Scanner(System.in);
		double[] borders = new double[3];
		double[] params = null;
		try {
			System.out.print("Количество параметров для функции: ");
			int params_count = Integer.parseInt(scanner.nextLine());
			if (params_count > 0) {
				params = new double[params_count];
				for (int i = 0; i < params_count; i++) {
					System.out.print(">>");
					params[i] = Double.parseDouble(scanner.nextLine());
				}
			} else {
				params = new double[0];
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			System.out.println("No params for function");
		}
		Arrays.toString(params);
		int opt = 0;
		try {
			do {
				System.out.println(METHODS);
				opt = Integer.parseInt(scanner.nextLine());
				switch (opt) {
				case 1:
					System.out.println("-=====================-");
					fillParams(borders, scanner);
					Methods.roots(borders[0], borders[1], borders[2], params);
					System.out.println("-=====================-");
					break;
				case 2:
					System.out.println("-=====================-");
					fillParams(borders, scanner);
					System.out.println("корень: " + Methods.method_chord(borders[0], borders[1], borders[2], params));
					System.out.println("-=====================-");
					break;
				case 3:
					System.out.println("-=====================-");
					fillParams(borders, scanner);
					System.out.println("корень: " + Methods.dixotomia(borders[0], borders[1], borders[2], params));
					System.out.println("-=====================-");
					break;
				case 0:
					break;
				default:
					System.out.println("-=====================-");
					System.out.println("smth wrong");
					System.out.println("-=====================-");
					break;
				}
			} while (opt != 0);
		} catch (NumberFormatException e) {
			System.out.println("Wrong input");
		}

		scanner.close();
	}

	public static double[] fillParams(double[] params, Scanner scaner) {
		try {
			System.out.println("От");
			params[0] = Double.parseDouble(scaner.nextLine());
			System.out.println("До");
			params[1] = Double.parseDouble(scaner.nextLine());
			System.out.println("Шаг");
			params[2] = Double.parseDouble(scaner.nextLine());
			return params;
		} catch (NumberFormatException e) {
			System.out.println("Wrong input");
		}
		return new double[] { 0, 0, 0 };
	}
	
}
