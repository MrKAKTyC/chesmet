package max.func;

public class Gaus {

	private int size;
	private double[][] arr;
	private double[] ansv;

	private double b_div_a;

	public Gaus(int size) {
		this.size = size;
		this.arr = new double[this.size][this.size + 1];
		this.ansv = new double[this.size];
	}

	public double[] calculate() {
		this.forward();
		this.backward();
		return this.ansv;
	}

	private void forward() {
		for (int a = 0; a < arr.length - 1; a++) {
			for (int b = a + 1; b < arr.length; b++) {
				this.b_div_a = (arr[b][a] / arr[a][a]);
				for (int cur = 0; cur < arr[0].length; cur++) {
					arr[b][cur] = arr[a][cur] * (-b_div_a) + arr[b][cur];
				}
			}
		}
	}

	private void backward() {
		for(int i = arr.length-1; i >= 0; i--) {
			double sum = 0;
			for(int j = arr.length-1;j >i ;j--) {
				sum += arr[i][j] * ansv[j];
			}
			ansv[i] = (arr[i][arr.length] - sum)/arr[i][i];
		}
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				builder.append(this.arr[i][j] + "\t");
			}
			builder.append("\n");
		}
		for (int i = 0; i < ansv.length; i++) {
			builder.append("[x" + i + " = " + ansv[i] + "]\n");
		}
		return builder.toString();
	}

	public void setArr(double arr[][]) {
		this.arr = arr;
	}
	
	public int getSize() {
		return this.size;
	}

	public static void main(String[] args) {
		double[][] test = new double[][] { { -2, 1, -4, 3 }, { -4, -2, -3, 11 }, { 5, 1, 8, 9 } };
		Gaus g = new Gaus(3);
		g.setArr(test);
		g.calculate();
		System.out.println(g.toString());
	}

}
