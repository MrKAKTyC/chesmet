package max.func;

public class Integral {
	private double from;
	private double to;
	private double count;
	private double acur;
	private IFunction func;
	private double result;

	public Integral(double from, double to, double count, IFunction func) {
		this.from = from;
		this.to = to;
		this.count = count;
		this.func = func;
	}

	public double calcalate() {
		this.acur = (to - from) / count;
		for (int i = 0; i < count; i++) {
			result += func.calculate(from + acur * (i + 0.5));
		}
		result *= acur;
		return this.result;
	}
	
	public double getResult() {
		return this.result;
	}
	
	public static void main(String[] args) {
		Integral i = new Integral(0, 10, 100, new IFunction() {
			@Override
			public double calculate(double x) {
				return Math.sin(x);
			}});
		System.out.println(i.calcalate());
	}

}
